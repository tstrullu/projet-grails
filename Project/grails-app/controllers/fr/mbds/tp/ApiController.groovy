package fr.mbds.tp

import grails.converters.JSON
import grails.converters.XML

import javax.servlet.http.HttpServletRequest


class ApiController {

    def index() { render "ok" }

    def responseFormat(Object instance, HttpServletRequest request) {
        switch (request.getHeader("Accept")) {
            case "test/xml":
                render instance as XML
                break
            case "test/json":
                render instance as JSON
                break
        }
    }

    def responseFormatList(List List, HttpServletRequest request) {
        switch (request.getHeader("Accept")) {
            case "test/xml":
                render List as XML
                break
            case "test/json":
                render List as JSON
                break
        }
    }


    def message() {
        switch (request.getMethod()) {
            case "GET":
                if (params.id) {
                    def messageInstance = Message.get(params.id)
                    if (messageInstance) {
                        responseFormat(messageInstance, request)
                    } else {
                        response.status = 404
                    }
                }
            case "POST":
                forward action: "messages"
                break

            case "PUT":
                def messageInstance = params.id ? Message.get(params.id) : null
                if (messageInstance) {
                    if (params.get("author.id")) {
                        def authorInstance = User.get((Long) params.get("author.id"))
                        if (authorInstance)
                            messageInstance.author = authorInstance
                    }
                    if (params.messageContent)
                        messageInstance.messageContent = params.messageContent
                    if (messageInstance.save(flush: true))
                        render(text: "Mise à jour effectuée pour le message ${MessageInstance.id}")
                    else
                        render(status: 400, text: "Echec de la mise a jour du message ${MessageInstance.id}")
                } else
                    render(status: 404, text: " Le message designe est introuvable")
                break
            case "DELETE":
                def messageInstance = params.id ? Message.get(params.id) : null
                if (messageInstance) {
                    def userMessages = UserMessage.findAllByMessage(messageInstance)
                    userMessages.each { UserMessage userMessage ->
                        userMessage.delete(flush: true)
                    }
                    messageInstance.delete(flush: true)
                    render(status: 200, text: "Message effacé")
                } else
                    render(status: 404, text: " Message introuvable")
                break
            default:
                response.status = 405
                break
        }
    }

    def messages() {
        switch (request.getMethod()) {
            case "GET":
                responseFormatList(Message.list(), request)
                break
            case "POST":
                def authorInstance = params.get("author.id") ? User.get(params.get("author.id")) : null
                def messageInstance
                if (authorInstance) {
                    messageInstance = new Message(author: authorInstance, messageContent: params.messageContent)
                    if (messageInstance.save(flush: true)) {
                        if (params.get("receiver.id")) {
                            def receiverInstance = (User.get(params.get("receiver.id")))
                            if (receiverInstance)
                                new UserMessage(user: receiverInstance, message: messageInstance).save(flush: true)
                        }
                    }
                    render(status: 201, message: "le message a été créé")
                }



                if (response.status != 201) {
                    response.status = 400
                }

                break
        }
    }

    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (params.id) {
                    def userInstance = User.get(params.id)
                    if (userInstance) {
                        responseFormat(userInstance, request)
                    } else {
                        response.status = 404
                    }
                }
            case "POST":
                forward action: "users"
                break

            case "PUT":
                def userInstance = params.id ? User.get(params.id) : null
                if (userInstance) {
                    if (params.get("username")) {
                        userInstance.username = params.get("username")
                    }
                    if (params.get("mail")) {
                        userInstance.mail = params.get("mail")
                    }
                    if (params.get("password")) {
                        userInstance.password = params.get("password")
                    }
                    if (params.get("firstName")) {
                        userInstance.firstName = params.get("firstName")
                    }
                    if (params.get("lastName")) {
                        userInstance.lastName = params.get("lastName")
                    }
                    if (userInstance.save(flush: true))
                        render(text: "Mise à jour effectuée pour l'utilisateur ${userInstance.id}")
                    else
                        render(status: 400, text: "Echec de la mise a jour de l'utilisateur ${userInstance.id}")
                } else
                    render(status: 404, text: " L'utilisateur designe est introuvable")
                break
            case "DELETE":
                def userInstance = params.id ? User.get(params.id) : null
                if (userInstance) {
                    userInstance.isDeleted = false
                    userInstance.delete(flush: true)
                    render(status: 200, text: "L'utilisateur a été supprimé")
                } else
                    render(status: 404, text: " Utilisateur introuvable")
                break
            default:
                response.status = 405
                break
        }
    }

    def users() {
        switch (request.getMethod()) {
            case "GET":
                responseFormatList(User.list(), request)
                break
            case "POST":
                def userInstance
                userInstance = new User(password: params.get("password"), username: params.get("username"), mail: params.get("mail"), dob: params.dob, tel: params.get("tel"), firstName: params.get("firstName"), lastName: params.get("lastName"), isDeleted: params.get("isDeleted"))
                if (userInstance.save(flush: true)) {
                    render(status: 201, text: "Nouvel utilisateur ${userInstance.id} créé")
                }
                if (response.status != 201)
                    response.status = 400
                break;
        }
    }

    def messageToUser() {
        switch (request.getMethod()) {
            case "POST":
                if (params.get("user.id")) {
                    def userInstance = User.get(params.get("user.id"))
                    if (userInstance) {
                        def messageInstance;
                        if (params.get("message.id")) {
                            messageInstance = Message.get(params.get("message.id"))
                        } else {
                            def authorInstance = params.get("author.id") ? User.get(params.get("author.id")) : null
                            if (authorInstance)
                                messageInstance = new Message(author: authorInstance, messageContent: params.messageContent).save(flush: true)
                        }
                        if(messageInstance) {
                            def userMessageInstance =  new UserMessage(message: messageInstance, user: userInstance)
                            if(userMessageInstance.save(flush: true))
                                render(status: 201, text: "Attribution du message ${messageInstance.id} à l'utilisateur ${userInstance.id} réussie.")
                        } else {
                            render(status: 400, text: "Message non récupéré - ")
                        }
                    } else {
                        render(status: 404, text: "utilisateur introuvable - ")
                    }
                }

                if (response.status != 201)
                    render(status: 400, text: "Message non attribué")

            default:
                response.status = 405
                break
        }
    }

    def messageToGroup() {
        switch (request.getMethod()) {
            case "POST":
                if (params.get("group.id")) {
                    def roleInstance = Role.get(params.group.id)
                    def userRoleList = UserRole.findAllByRole(roleInstance)
                    def userList = userRoleList.collect{ it.user }
                    if (roleInstance) {
                        def messageInstance;
                        if (params.get("message.id")) {
                            messageInstance = Message.get(params."message.id")
                        } else {
                            def authorInstance = params.get("author.id") ? User.get(params.author.id) : null
                            if (authorInstance)
                                messageInstance = new Message(author: authorInstance, messageContent: params.messageContent).save(flush: true)
                        }
                        if(messageInstance) {
                            for (user in userList) {
                                def userMessageInstance =  new UserMessage(message: messageInstance, user: user)
                                if(userMessageInstance.save(flush: true))
                                    render(status: 201, text: "Attribution du message ${messageInstance.id} à l'utilisateur ${user.id} réussie. ")
                            }
                        } else {
                            render(status: 400, text: "Message non récupéré - ")
                        }
                    } else {
                        render(status: 404, text: "Groupe introuvable - ")
                    }
                }

                if (response.status != 201)
                    render(status: 400, text: "Message non attribué")

            default:
                response.status = 405
                break
        }
    }
}