<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>
    <asset:image src="img.jpg" class="grails-logo"/>
    %{--<div class="svg" role="presentation">
        <div class="grails-logo-container">
            <asset:image src="grails-cupsonly-logo-white.svg" class="grails-logo"/>
        </div>
    </div> --}%

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h1>Messagerie Instantanée</h1>

            <p>
                Bienvenue sur cette messagerie instantanée. Vous pouvez découvrir les différentes options en cliquant sur
                les onglets Utilisateurs, Messages, et Groupe. Bonne continuation.
            </p>

            %{--}<div id="controllers" role="navigation">
                <h2>Available Controllers:</h2>
                <ul>
                    <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
                        <li class="controller">
                            <g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link>
                        </li>
                    </g:each>
                </ul>
            </div>--}%
        </section>
    </div>

</body>
</html>
