package fr.mbds.tp

import grails.gorm.services.Service

@Service(UserMessage)
interface UserMessageService {

    UserMessage get(Serializable id)

    List<UserMessage> list(Map args)

    Long count()

    void delete(Serializable id)

    UserMessage save(UserMessage userMessage)

}