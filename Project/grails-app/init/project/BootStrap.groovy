package project

import fr.mbds.tp.Message
import fr.mbds.tp.Role
import fr.mbds.tp.User
import fr.mbds.tp.UserMessage
import fr.mbds.tp.UserRole

class BootStrap {

    def init = { servletContext ->
        def userAdmin = new User (username: "Admin", password: "secret", firstName: "admin", lastName: "admin", mail: "email_admin").save(flush: true, failOnError: true)
        def roleAdmin = new Role (authority: "ROLE_ADMIN").save(flush: true, failOnError: true)
        def roleUser = new Role (authority: "ROLE_USERS").save(flush: true, failOnError: true)
        UserRole.create(userAdmin, roleAdmin, true)
        (1..50).each {
            def userInstance = new User(username: "username-$it", password: "password", firstName: "first", lastName: "last", mail: "email-$it").save(flush: true, failOnError: true)
            new Message(messageContent: "lalalala", author: userInstance).save(flush: true)
            UserRole.create(userInstance, roleUser, true)
        }
        Message.list().each{
            Message messageInstance ->
                User.list().each{
                    User userInstance ->
                        UserMessage.create(userInstance, messageInstance,true)
                }
        }

    }
    def destroy = {
    }
}
